class CreateParagelia < ActiveRecord::Migration
  def change
    create_table :paragelia do |t|
      t.text :drinks
      t.text :food

      t.timestamps
    end
  end
end

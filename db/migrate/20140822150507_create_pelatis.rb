class CreatePelatis < ActiveRecord::Migration
  def change
    create_table :pelatis do |t|
      t.string :onoma
      t.string :epitheto

      t.timestamps
    end
  end
end

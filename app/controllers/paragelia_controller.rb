class ParageliaController < ApplicationController
  before_action :set_paragelium, only: [:show, :edit, :update, :destroy]

  # GET /paragelia
  # GET /paragelia.json
  def index
    @paragelia = Paragelium.all
  end

  # GET /paragelia/1
  # GET /paragelia/1.json
  def show
  end

  # GET /paragelia/new
  def new
    @paragelium = Paragelium.new
  end

  # GET /paragelia/1/edit
  def edit
  end

  # POST /paragelia
  # POST /paragelia.json
  def create
    @paragelium = Paragelium.new(paragelium_params)

    respond_to do |format|
      if @paragelium.save
        format.html { redirect_to @paragelium, notice: 'Paragelium was successfully created.' }
        format.json { render :show, status: :created, location: @paragelium }
      else
        format.html { render :new }
        format.json { render json: @paragelium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paragelia/1
  # PATCH/PUT /paragelia/1.json
  def update
    respond_to do |format|
      if @paragelium.update(paragelium_params)
        format.html { redirect_to @paragelium, notice: 'Paragelium was successfully updated.' }
        format.json { render :show, status: :ok, location: @paragelium }
      else
        format.html { render :edit }
        format.json { render json: @paragelium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paragelia/1
  # DELETE /paragelia/1.json
  def destroy
    @paragelium.destroy
    respond_to do |format|
      format.html { redirect_to paragelia_url, notice: 'Paragelium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paragelium
      @paragelium = Paragelium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paragelium_params
      params.require(:paragelium).permit(:drinks, :food)
    end
end

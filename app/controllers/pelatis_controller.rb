class PelatisController < ApplicationController
  before_action :set_pelati, only: [:show, :edit, :update, :destroy]

  # GET /pelatis
  # GET /pelatis.json
  def index
    @pelatis = Pelati.all
  end

  # GET /pelatis/1
  # GET /pelatis/1.json
  def show
  end

  # GET /pelatis/new
  def new
    @pelati = Pelati.new
  end

  # GET /pelatis/1/edit
  def edit
  end

  # POST /pelatis
  # POST /pelatis.json
  def create
    @pelati = Pelati.new(pelati_params)

    respond_to do |format|
      if @pelati.save
        format.html { redirect_to @pelati, notice: 'Pelati was successfully created.' }
        format.json { render :show, status: :created, location: @pelati }
      else
        format.html { render :new }
        format.json { render json: @pelati.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pelatis/1
  # PATCH/PUT /pelatis/1.json
  def update
    respond_to do |format|
      if @pelati.update(pelati_params)
        format.html { redirect_to @pelati, notice: 'Pelati was successfully updated.' }
        format.json { render :show, status: :ok, location: @pelati }
      else
        format.html { render :edit }
        format.json { render json: @pelati.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pelatis/1
  # DELETE /pelatis/1.json
  def destroy
    @pelati.destroy
    respond_to do |format|
      format.html { redirect_to pelatis_url, notice: 'Pelati was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pelati
      @pelati = Pelati.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pelati_params
      params.require(:pelati).permit(:onoma, :epitheto)
    end
end

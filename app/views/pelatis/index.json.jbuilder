json.array!(@pelatis) do |pelati|
  json.extract! pelati, :id, :onoma, :epitheto
  json.url pelati_url(pelati, format: :json)
end

json.array!(@paragelia) do |paragelium|
  json.extract! paragelium, :id, :drinks, :food
  json.url paragelium_url(paragelium, format: :json)
end

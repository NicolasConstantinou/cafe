require 'test_helper'

class PelatisControllerTest < ActionController::TestCase
  setup do
    @pelati = pelatis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pelatis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pelati" do
    assert_difference('Pelati.count') do
      post :create, pelati: { epitheto: @pelati.epitheto, onoma: @pelati.onoma }
    end

    assert_redirected_to pelati_path(assigns(:pelati))
  end

  test "should show pelati" do
    get :show, id: @pelati
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pelati
    assert_response :success
  end

  test "should update pelati" do
    patch :update, id: @pelati, pelati: { epitheto: @pelati.epitheto, onoma: @pelati.onoma }
    assert_redirected_to pelati_path(assigns(:pelati))
  end

  test "should destroy pelati" do
    assert_difference('Pelati.count', -1) do
      delete :destroy, id: @pelati
    end

    assert_redirected_to pelatis_path
  end
end

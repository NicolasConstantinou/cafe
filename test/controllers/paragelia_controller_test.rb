require 'test_helper'

class ParageliaControllerTest < ActionController::TestCase
  setup do
    @paragelium = paragelia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:paragelia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create paragelium" do
    assert_difference('Paragelium.count') do
      post :create, paragelium: { drinks: @paragelium.drinks, food: @paragelium.food }
    end

    assert_redirected_to paragelium_path(assigns(:paragelium))
  end

  test "should show paragelium" do
    get :show, id: @paragelium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @paragelium
    assert_response :success
  end

  test "should update paragelium" do
    patch :update, id: @paragelium, paragelium: { drinks: @paragelium.drinks, food: @paragelium.food }
    assert_redirected_to paragelium_path(assigns(:paragelium))
  end

  test "should destroy paragelium" do
    assert_difference('Paragelium.count', -1) do
      delete :destroy, id: @paragelium
    end

    assert_redirected_to paragelia_path
  end
end
